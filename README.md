# Morse Code - Mercado Libre S.R.L

Demo to translate Morse to human and backwards

Tiene 3 funciones:

1) Una función decodeBits2Morse que dada una secuencia de bits, retorne un string con el resultado en MORSE.

2) Una función translateToText que tome el string en MORSE y retorne un string 
legible por un humano

3) Una función translateToMorse que tome un string legible por un humano
y lo transforme a morse


[Link de referencia a challege Original](CHALLENGE.md)


### Bonus: 

1. Diseñar una API que permita traducir texto de MORSE a lenguaje humano y 
visceversa. 

2. Hostear la API en un cloud público (como app engine o cloud foundry) y enviar la 
URL para consulta 

Ejemplo:
``` 
$ curl -X POST "http://meli.com/translate/2text" -d "{text: '.... --- .-.. .- -- . .-.. ..'}" 
{ code:200, response: 'HOLA MELI'} 
$ curl -X POST "http://meli.com/translate/2morse" -d "{text: 'HOLA MELI'}" { code:200, response: '.... --- .-.. .- -- . .-.. ..'} 
```

### Tabla de Translations

![alt text](blob/tableMorse.png)


# Resources
- Java 8
- Spring boot 2.3.1 
- Maven 3.6.0
- Docker 19.03.1
- Gcloud
- Sonarqueue


### Extra

- add sentry
- add sonarqueue
- add jenkins

# Contact
Autor: Manuel Jose Sotomayor Torrealba
Email: manuelen12@gmail.com
Domain: morce.cloudemotion.com
date: 20-06-2020


# how to run for dev
Para Empezar podemos simplemente utilizar docker y levantar el servicio
``` 
$ docker-compose -f local.yml build
$ docker-compose -f local.yml up
``` 

en caso de no tener Docker tambien podemos utilizar mvn
``` 
$ mvn install
$ mvn spring-boot:run
``` 

Luego de ellos Simplemente Ingreamos en y podemos ver la documentacion en swagger y como usarlo
 

http://localhost:8080/swagger-ui.html#/

# Excecute test
executa el comando con docker
``` 
$ docker-compose -f local.yml run mvn test sonar:sonar -Dsonar.host.url=http://localhost:5000 -Dsonar.login=admin -Dsonar.password=admin
``` 
o con mvn
``` 
$ mvn test sonar:sonar -Dsonar.host.url=http://localhost:5000 -Dsonar.login=admin -Dsonar.password=admin
``` 

En caso de haber levantado el proyecto con docker automaticamente tendra la posibilidad de ver el coverage y otras cosas en localhost:5000

# Proyecto en servidor publico con kubernetes
En caso de quere ver el proyecto publico unicamente ingresas en:

http://mercadolibre.cloudemotionteam.com:8080/swagger-ui.html#/


#Cosas a tomar en cuenta
### Texto2Morse
- El punto (".") identifica el final en la api de texto2morse, (no se traducira mas texto despues del punto).
- No se aceptan Caracteres especiales en texto2morse
- No se permitiran codigos morse no existentes en la tabla
- Se traducira un espacio como 2 espacios en morse

### Morse2Texto
- 2 Espacios en morse identifican un espacio en texto
- De Morse2texto siempre volvera en Mayusculas
- De Morse2Texto el final se identifica con (".-.-.-", nose traducira mas despues de ese codigo)
- de Morse2Text solo se aceptaran (Espacio(" "), Punto(".") y signo de menos ("-") )

# Referencias
 
 - [Link de Referencias de spring](HELP.md)
 - [Trello de Actividades](https://trello.com/b/GixM15re/test)