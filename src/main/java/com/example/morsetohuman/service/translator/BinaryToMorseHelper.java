package com.example.morsetohuman.service.translator;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Translating String to Text using the MAP in the Main Class
 * Author: manuelen12
 */


@Service
@Slf4j
public class BinaryToMorseHelper extends TranslatorHelper {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public String textTranslated(List<String> texts) {
        String finalText = "";
        for (String text: texts){
            finalText = finalText.concat(PULSE_MORSE.get(text));
        }
        logger.info("Translate to Morse {}", finalText);
        return finalText;

    }

}
