package com.example.morsetohuman.service.translator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Main Class To Translate and inherit the Class
 * There are two CONTANTS To translate the STRING
 * Author: manuelen12
 */

public abstract class TranslatorHelper {
    public abstract String textTranslated(List<String> message);

    protected static final Map<String, String> ALPHABET_TEXT = new HashMap<>();
    protected static final Map<String, String> ALPHABET_MORSE = new HashMap<>();
    protected static final Map<String, String> PULSE_MORSE = new HashMap<>();

    static        {
        // MAP To TEXT TO MORSE
        ALPHABET_TEXT.put("A", ".-");
        ALPHABET_TEXT.put("B", "-...");
        ALPHABET_TEXT.put("C", "-.-.");
        ALPHABET_TEXT.put("D", "-..");
        ALPHABET_TEXT.put("E", ".");
        ALPHABET_TEXT.put("F", "..-.");
        ALPHABET_TEXT.put("G", "--.");
        ALPHABET_TEXT.put("H", "....");
        ALPHABET_TEXT.put("I", "..");
        ALPHABET_TEXT.put("J", ".---");
        ALPHABET_TEXT.put("K", "-.-");
        ALPHABET_TEXT.put("L", ".-..");
        ALPHABET_TEXT.put("M", "--");
        ALPHABET_TEXT.put("N", "-.");
        ALPHABET_TEXT.put("O", "---");
        ALPHABET_TEXT.put("P", ".--.");
        ALPHABET_TEXT.put("Q", "--.-");
        ALPHABET_TEXT.put("R", ".-.");
        ALPHABET_TEXT.put("S", "...");
        ALPHABET_TEXT.put("T", "-");
        ALPHABET_TEXT.put("U", "..-");
        ALPHABET_TEXT.put("V", "...-");
        ALPHABET_TEXT.put("W", ".--");
        ALPHABET_TEXT.put("X", "-..-");
        ALPHABET_TEXT.put("Y", "-.--");
        ALPHABET_TEXT.put("Z", "--..");
        ALPHABET_TEXT.put("0", "-----");
        ALPHABET_TEXT.put("1", ".----");
        ALPHABET_TEXT.put("2", "..---");
        ALPHABET_TEXT.put("3", "...--");
        ALPHABET_TEXT.put("4", "....-");
        ALPHABET_TEXT.put("5", ".....");
        ALPHABET_TEXT.put("6", "-....");
        ALPHABET_TEXT.put("7", "--...");
        ALPHABET_TEXT.put("8", "---..");
        ALPHABET_TEXT.put("9", "----.");
        ALPHABET_TEXT.put(".", ".-.-.-");
        ALPHABET_TEXT.put(" ", "");

        // MAP TO CASE ADVERSE
        ALPHABET_TEXT.forEach((k, v) -> ALPHABET_MORSE.put(v, k));

        // MAP TO PULSE TO MOSE

        PULSE_MORSE.put("short_one", ".");
        PULSE_MORSE.put("long_one", "-");
        PULSE_MORSE.put("long_cero", " ");
        PULSE_MORSE.put("short_cero", "");
        PULSE_MORSE.put("end", "");

    }

}

