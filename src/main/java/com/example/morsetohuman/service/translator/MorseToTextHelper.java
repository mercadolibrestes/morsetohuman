package com.example.morsetohuman.service.translator;

import com.example.morsetohuman.util.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Translating String to Text using the MAP in the Main Class
 * Author: manuelen12
 */


@Service
@Slf4j
public class MorseToTextHelper extends TranslatorHelper {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public String textTranslated(List<String> texts){
        String finalText = "";
        for (String text: texts){
            String textAppend = ALPHABET_MORSE.getOrDefault(text, "");
            if (textAppend.isEmpty()) {
                throw new ApiException(String.format("The %s is a invalid Morse", text));
            }
            finalText = finalText.concat(textAppend);
            if (text.equals(".-.-.-")){
                break;
            }
        }
        logger.info("Translate to Text {}", finalText);
        return finalText;

    }

}
