package com.example.morsetohuman.service;

import com.example.morsetohuman.controller.dto.TranslateRequest;
import com.example.morsetohuman.controller.dto.TranslateResponse;
import com.example.morsetohuman.service.parser.BinaryParser;
import com.example.morsetohuman.service.parser.MorseParser;
import com.example.morsetohuman.service.parser.TextParser;
import com.example.morsetohuman.service.translator.BinaryToMorseHelper;
import com.example.morsetohuman.service.translator.MorseToTextHelper;
import com.example.morsetohuman.service.translator.TextToMorseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Translator Service, to make the work to Translate
 * Author: manuelen12
 */

@Service
public class TranslateService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private String logText = "Parse info to {}";


    // Parser to send info to Translator
    @Autowired
    private TextParser textParser;

    @Autowired
    private MorseParser morseParser;

    @Autowired
    private BinaryParser binaryParser;


    // Translator to change text from something to other
    @Autowired
    private TextToMorseHelper textToMorseHelper;

    @Autowired
    private MorseToTextHelper morseToTextHelper;

    @Autowired
    private BinaryToMorseHelper binaryToMorseHelper;

    // Methods to Translate
    public TranslateResponse translateToMorse(TranslateRequest translateRequest){
        List<String> texts =  textParser.parser(translateRequest.getText());
        logger.info(logText, texts);
        return  new TranslateResponse(textToMorseHelper.textTranslated(texts));
    }

    public TranslateResponse translateToText(TranslateRequest translateRequest){
        List<String> morseText =  morseParser.parser(translateRequest.getText());
        logger.info(logText, morseText);
        return new TranslateResponse(morseToTextHelper.textTranslated(morseText));
    }

    public TranslateResponse decodeBitsToMorse(TranslateRequest translateRequest){
        List<String> binaryText =  binaryParser.parser(translateRequest.getText());
        logger.info(logText, binaryText);
        return new TranslateResponse(binaryToMorseHelper.textTranslated(binaryText));
    }
    
}