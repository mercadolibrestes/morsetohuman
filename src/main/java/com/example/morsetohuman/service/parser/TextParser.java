package com.example.morsetohuman.service.parser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Class Parse To prepare String To transform to Morse
 * Author: manuelen12
 */

@Service
@Slf4j
public class TextParser extends MainParser {
    @Override
    public List<String> parser(String text) {

        return text.chars().mapToObj(c -> Character.toString((char) c).toUpperCase()).collect(Collectors.toList());

    }
}
