package com.example.morsetohuman.service.parser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Class Parse To prepare String To transform to text
 * Author: manuelen12
 */

@Service
@Slf4j
public class MorseParser extends MainParser {
    @Override
    public List<String> parser(String text) {

        return Arrays. asList(text. split(" "));

    }
}
