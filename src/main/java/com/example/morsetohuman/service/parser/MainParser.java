package com.example.morsetohuman.service.parser;

import java.util.List;

/**
 * Main Parser to inherance
 * Author: manuelen12
 */

public abstract class MainParser {
    public abstract List<String> parser(String message);
}
