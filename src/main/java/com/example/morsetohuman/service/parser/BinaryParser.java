package com.example.morsetohuman.service.parser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class Parse To prepare String To transform to text
 * Author: manuelen12
 */

@Service
@Slf4j
public class BinaryParser extends MainParser {
    private Integer longPulse;

    @Override
    public List<String> parser(String text) {
        List<String> binaryText = new ArrayList<>();
        List<Integer> binaryCero = new ArrayList<>();
        List<Integer> binaryOne = new ArrayList<>();

        List<String> maxzero = Arrays.asList(text.replaceAll("(1+)", " ").trim().split(" "));
        this.longPulse = maxzero.stream().map(String::length).max(Integer::compareTo).orElse(0);

        Matcher m = Pattern.compile("(0+|1+)").matcher(
                text.replace("0", " ").trim().replace(" ", "0"));

        while(m.find()) {
            String binary = m.group();
            Character typeBit = binary.charAt(0);
            binaryText.add(binary);
            if (typeBit.equals('1')){
                binaryOne.add(binary.length());
            }else{
                binaryCero.add(binary.length());
            }
        }
        return calculateAverage(binaryText, binaryOne, binaryCero);
    }

    private List<String> calculateAverage(List<String> binaryText, List<Integer>binaryOne, List<Integer> binaryCero) {
        Integer maxCero = binaryCero.stream().max(Integer::compareTo).orElse(0);
        Integer minCero = binaryCero.stream().min(Integer::compareTo).orElse(0);
        Integer maxOne = binaryOne.stream().max(Integer::compareTo).orElse(0);
        Integer minOne =  binaryOne.stream().min(Integer::compareTo).orElse(0);

        List<String> typePulses = new ArrayList<>();
        for (String bit: binaryText){
            typePulses.add(pulseType(bit, maxCero,minCero, maxOne, minOne));
        }
        return typePulses;

    }
    
    private String pulseType(String binary, Integer maxCero, Integer minCero, Integer maxOne, Integer minOne){
        String pulses = null;

        if (binary != null && binary.length() > 0) {
            Character pulseType = binary.charAt(0);

            if (pulseType.equals('1')) {
                if (binary.length() < ((maxOne + minOne) / 2)) {
                    pulses = "short_one";
                } else {
                    pulses = "long_one";
                }
            } else if (pulseType.equals('0')) {
                if (binary.length() < ((maxCero + minCero) / 2)) {
                    pulses = "short_cero";
                }else if(binary.length() == this.longPulse){
                    pulses = "end";
                } else {
                    pulses = "long_cero";
                }
            }
        }

        return pulses;
    } 

}
