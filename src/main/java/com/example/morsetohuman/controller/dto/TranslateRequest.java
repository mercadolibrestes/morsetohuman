package com.example.morsetohuman.controller.dto;

import com.example.morsetohuman.controller.validator.GroupBit;
import com.example.morsetohuman.controller.validator.GroupMorse;
import com.example.morsetohuman.controller.validator.GroupText;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * DTO to Receive the requets
 * Author: manuelen12
 */


public class TranslateRequest {

    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

    // Create 3 interface to valid with diferents form using pattern in diferent groups
    @NotBlank(message = "Text is Mandatory")
    @Pattern(regexp = "[a-zA-Z0-9. ]+", groups = GroupText.class)
    @Pattern(regexp = "[ .-]+", groups = GroupMorse.class)
    @Pattern(regexp = "[0-1]+", groups = GroupBit.class)
    private String text;
}
