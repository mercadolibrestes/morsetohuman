package com.example.morsetohuman.controller.dto;

/**
 * DTO To Response the Request
 * Author: manuelen12
 */

public class TranslateResponse {

    private final String text;

    public TranslateResponse(String text) {
        this.text = text;
    }
    
    public String getText() {
        return text;
    }
}
