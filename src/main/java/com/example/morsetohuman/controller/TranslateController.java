package com.example.morsetohuman.controller;

import com.example.morsetohuman.controller.dto.TranslateRequest;
import com.example.morsetohuman.controller.dto.TranslateResponse;
import com.example.morsetohuman.controller.validator.GroupBit;
import com.example.morsetohuman.controller.validator.GroupMorse;
import com.example.morsetohuman.controller.validator.GroupText;
import com.example.morsetohuman.service.TranslateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.groups.Default;

/**
 * Controller para Manejar las peticiones
 * Author: manuelen12
 */

@RestController
@RequestMapping(value = "/translate")
@Api(tags = "Translate")
public class TranslateController {
    private final Logger logger = LoggerFactory.getLogger(TranslateController.class);

    // Declaro los servicios

    @Autowired
    private TranslateService translateService;


    // Declaro las rutas

    @PostMapping(path = "/2text")
    @ApiOperation(value = "From Morse To Text", notes = "Translate from Morse To Text")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Translation Successfull"),
            @ApiResponse(code = 400, message = "Invalid request")
    })
    public TranslateResponse translateToText(@Validated({Default.class, GroupMorse.class}) @RequestBody TranslateRequest translateRequest) {
        logger.info("Receive Text: {}", translateRequest.getText());
        return translateService.translateToText(translateRequest);

    }


    @PostMapping(path = "/2morse")
    @ApiOperation(value = "From Text To Morse", notes = "Translate from Text To Morse")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Translation Successfull"),
            @ApiResponse(code = 400, message = "Invalid request")
    })
    public TranslateResponse translateToMorse(@Validated({Default.class, GroupText.class}) @RequestBody TranslateRequest translateRequest) {
        logger.info("Receive Morse: {}", translateRequest.getText());
        return translateService.translateToMorse(translateRequest);
    }


    @PostMapping(path = "/bits2morse")
    @ApiOperation(value = "From Bit To Morse", notes = "Translate from Bits To Morse")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Translation Successfull"),
            @ApiResponse(code = 400, message = "Invalid request")
    })
    public TranslateResponse decodeBits2Morse(@Validated({Default.class, GroupBit.class}) @RequestBody TranslateRequest translateRequest) {
        logger.info("Receive BIT: {}", translateRequest.getText());
        return translateService.decodeBitsToMorse(translateRequest);
    }


}
