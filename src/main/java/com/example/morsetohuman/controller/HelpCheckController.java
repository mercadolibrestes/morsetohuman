package com.example.morsetohuman.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Help Check to check any down of the service
 */
@RestController
@RequestMapping(value = "/help")
@Api(tags = "help_check")
public class HelpCheckController {

    private final Logger logger = LoggerFactory.getLogger(HelpCheckController.class);
    // Declaro las rutas


    @GetMapping(path = "/check")
    @ApiOperation(value = "Help Check", notes = "To valid ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Translation Successfull"),
            @ApiResponse(code = 400, message = "Invalid request")
    })
    public String helpCheck() {
        logger.info("HELP CHECK WORKING GOOG");
        return "";

    }


}
