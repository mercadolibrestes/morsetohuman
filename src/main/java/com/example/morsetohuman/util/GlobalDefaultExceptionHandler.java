package com.example.morsetohuman.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
class GlobalDefaultExceptionHandler {
    private ErrorResponse errorResponse = new ErrorResponse();

    @ExceptionHandler(value = ApiException.class)
    public ResponseEntity <ErrorResponse>
        controllerErrorException( ApiException e, HttpServletResponse response){
        errorResponse.setMessage(e.getLocalizedMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }
}