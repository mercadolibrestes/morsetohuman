package com.example.morsetohuman.util;

public class ApiException extends RuntimeException {

    public ApiException(String message) {
        super(message);
    }
}
