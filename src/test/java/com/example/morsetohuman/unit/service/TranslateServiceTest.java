package com.example.morsetohuman.unit.service;

import com.example.morsetohuman.controller.dto.TranslateRequest;
import com.example.morsetohuman.service.TranslateService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@SpringBootTest
@ExtendWith(SpringExtension.class)
class TranslateServiceTest {

    TranslateRequest translateRequest = new TranslateRequest();

    @Autowired
    private TranslateService translateService;


    // Text To Morse

    @Test
    public void test_hola_meli_with_space() {
        translateRequest.setText("HOLA MELI");

        Assertions.assertEquals(".... --- .-.. .-  -- . .-.. ..", translateService.translateToMorse(translateRequest).getText());
    }

    @Test
    public void test_lowercase_hola_meli_text_to_morse() {
        translateRequest.setText("holameli");

        Assertions.assertEquals(".... --- .-.. .- -- . .-.. ..", translateService.translateToMorse(translateRequest).getText());
    }

    @Test
    public void test_uppercase_hola_meli_text_to_morse() {
        translateRequest.setText("HOLA MELI");

        Assertions.assertEquals(".... --- .-.. .-  -- . .-.. ..", translateService.translateToMorse(translateRequest).getText());
    }

    @Test
    public void test_hola_meli_text_to_morse_with_3_spaces() {
        translateRequest.setText("HOLA   MELI");

        Assertions.assertEquals(".... --- .-.. .-    -- . .-.. ..", translateService.translateToMorse(translateRequest).getText());
    }


    @Test
    public void test_all_valid_text() {
        translateRequest.setText("1234567890 asdfghjklqwertyuiopzxcvbnm ASDFGHJKLQWERTYUIOPZXCVBNM.");

        Assertions.assertEquals(
                ".---- ..--- ...-- ....- ..... -.... --... ---.. ----. -----  .- ... -.. ..-. --. .... .--- -.- .-.. --.- .-- . .-. - -.-- ..- .. --- .--. --.. -..- -.-. ...- -... -. --  .- ... -.. ..-. --. .... .--- -.- .-.. --.- .-- . .-. - -.-- ..- .. --- .--. --.. -..- -.-. ...- -... -. -- .-.-.-",
                translateService.translateToMorse(translateRequest).getText());
    }

    @Test
    public void test_with_a_error() {
        Boolean error = false;
        translateRequest.setText("probando # ");
        try{
            translateService.translateToMorse(translateRequest);
        }catch (Exception e){
            error = true;
        }
        Assertions.assertTrue(error);

    }

    // Morse To test

    @Test
    public void test_hola_meli_morese_to_text_without_space() {
        translateRequest.setText(".... --- .-.. .- -- . .-.. ..");

        Assertions.assertEquals("HOLAMELI", translateService.translateToText(translateRequest).getText());
    }

    @Test
    public void test_hola_meli_morese_to_text_with_space() {
        translateRequest.setText(".... --- .-.. .-  -- . .-.. ..");

        Assertions.assertEquals("HOLA MELI", translateService.translateToText(translateRequest).getText());
    }

    @Test
    public void test_translate_all_morse_to_text() {
        translateRequest.setText(".---- ..--- ...-- ....- ..... -.... --... ---.. ----. -----  .- ... -.. ..-. --. .... .--- -.- .-.. --.- .-- . .-. - -.-- ..- .. --- .--. --.. -..- -.-. ...- -... -. --  .- ... -.. ..-. --. .... .--- -.- .-.. --.- .-- . .-. - -.-- ..- .. --- .--. --.. -..- -.-. ...- -... -. -- .-.-.-");

        Assertions.assertEquals("1234567890 ASDFGHJKLQWERTYUIOPZXCVBNM ASDFGHJKLQWERTYUIOPZXCVBNM.",
                translateService.translateToText(translateRequest).getText());
    }

    // Binary To Morse

    @Test
    public void test_hello_meli_binary_to_more_with_ceros() {
        translateRequest.setText("000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000");

        Assertions.assertEquals(".... --- .-.. .- -- . .-.. ..", translateService.decodeBitsToMorse(translateRequest).getText());
    }

    @Test
    public void test_hello_binary_to_more_with_ceros() {
        translateRequest.setText("000000000000110011011001100000011111110111111111110111111111000000000010111111110110011000000010111111100000000");

        Assertions.assertEquals(".... --- .-.. .-", translateService.decodeBitsToMorse(translateRequest).getText());
    }

    @Test
    public void test_hello_binary_to_more_without_ceros() {
        translateRequest.setText("1100110110011000000111111101111111111101111111110000000000101111111101100110000000101111111");

        Assertions.assertEquals(".... ---.-.. .-", translateService.decodeBitsToMorse(translateRequest).getText());
    }


    @Test
    public void test_example_binary() {
        translateRequest.setText("11111111110101011111110111");
        Assertions.assertEquals("-..-.", translateService.decodeBitsToMorse(translateRequest).getText());
    }

}
