package com.example.morsetohuman.end.to.end.controller;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class HelpCheckControllerTest {

    @Autowired
    private TestRestTemplate template;

    @LocalServerPort
    int randomServerPort;

    final String baseUrl = "/help/";

    @Test
    void helpCheck() {
        ResponseEntity<String> result = template.getForEntity(baseUrl+"check", String.class);
        Assertions.assertEquals(200, result.getStatusCodeValue());
    }
}