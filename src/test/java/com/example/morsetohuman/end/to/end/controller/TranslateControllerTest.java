package com.example.morsetohuman.end.to.end.controller;

import com.example.morsetohuman.controller.dto.TranslateRequest;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class TranslateControllerTest {
    @Autowired
    private TestRestTemplate template;

    @LocalServerPort
    int randomServerPort;

    final String baseUrl = "/translate/";

    // Validation MorsetoText
    @Test
    void test_translate_to_text_hello_meli(){
        TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText(".... --- .-.. .-  -- . .-.. ..");
        ResponseEntity<JsonNode> result = template.postForEntity(baseUrl+"2text", translateRequest, JsonNode.class);
        Assertions.assertEquals("\"HOLA MELI\"", result.getBody().get("text").toString() );
        Assertions.assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void test_translate_to_text_all(){
        TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText(".---- ..--- ...-- ....- ..... -.... --... ---.. ----. -----  .- ... -.. ..-. --. .... .--- -.- .-.. --.- .-- . .-. - -.-- ..- .. --- .--. --.. -..- -.-. ...- -... -. --  .- ... -.. ..-. --. .... .--- -.- .-.. --.- .-- . .-. - -.-- ..- .. --- .--. --.. -..- -.-. ...- -... -. -- .-.-.-");
        ResponseEntity<JsonNode> result = template.postForEntity(baseUrl+"2text", translateRequest, JsonNode.class);
        Assertions.assertEquals("\"1234567890 ASDFGHJKLQWERTYUIOPZXCVBNM ASDFGHJKLQWERTYUIOPZXCVBNM.\"", result.getBody().get("text").toString() );
        Assertions.assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void test_translate_to_text_invalid() {
        TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText("caracas");
        ResponseEntity<JsonNode> result = template.postForEntity(baseUrl+"2text", translateRequest, JsonNode.class);
        Assertions.assertEquals(400, result.getStatusCodeValue());
    }

    @Test
    void test_translate_to_text_morse_invalid() {
        TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText("..........");
        ResponseEntity<JsonNode> result = template.postForEntity(baseUrl+"2text", translateRequest, JsonNode.class);
        Assertions.assertEquals(400, result.getStatusCodeValue());
        Assertions.assertEquals("\"The .......... is a invalid Morse\"", result.getBody().get("message").toString());
    }

    @Test
    void test_translate_to_text_point_morse() {
        TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText("-.-. .- .-. .- -.-. .- ... .-.-.-  .... --- .-.. .-");
        ResponseEntity<JsonNode> result = template.postForEntity(baseUrl+"2text", translateRequest, JsonNode.class);
        Assertions.assertEquals(200, result.getStatusCodeValue());
        Assertions.assertEquals("\"CARACAS.\"", result.getBody().get("text").toString() );
    }


    // Validation TextToMorse
    @Test
    void test_translate_to_morse_hello_meli(){
        TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText("Hola Meli");
        ResponseEntity<JsonNode> result = template.postForEntity(baseUrl+"2morse", translateRequest, JsonNode.class);
        Assertions.assertEquals("\".... --- .-.. .-  -- . .-.. ..\"", result.getBody().get("text").toString() );
        Assertions.assertEquals(200, result.getStatusCodeValue());
    }
    @Test
    void test_translate_to_morse_all(){
        TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText("1234567890 asdfghjklqwertyuiopzxcvbnm ASDFGHJKLQWERTYUIOPZXCVBNM.");
        ResponseEntity<JsonNode> result = template.postForEntity(baseUrl+"2morse", translateRequest, JsonNode.class);
        Assertions.assertEquals("\".---- ..--- ...-- ....- ..... -.... --... ---.. ----. -----  .- ... -.. ..-. --. .... .--- -.- .-.. --.- .-- . .-. - -.-- ..- .. --- .--. --.. -..- -.-. ...- -... -. --  .- ... -.. ..-. --. .... .--- -.- .-.. --.- .-- . .-. - -.-- ..- .. --- .--. --.. -..- -.-. ...- -... -. -- .-.-.-\"", result.getBody().get("text").toString() );
        Assertions.assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void test_translate_to_morse_invalid() {
        TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText("ñ");
        ResponseEntity<JsonNode> result = template.postForEntity(baseUrl+"2morse", translateRequest, JsonNode.class);
        Assertions.assertEquals(400, result.getStatusCodeValue());
    }

    @Test
    void test_translate_to_morse_text_invalid() {
        TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText("#");
        ResponseEntity<JsonNode> result = template.postForEntity(baseUrl+"2morse", translateRequest, JsonNode.class);
        Assertions.assertEquals(400, result.getStatusCodeValue());
    }

    @Test
    void test_translate_to_morse_point_text() {
        TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText("Caracas. de noche");
        ResponseEntity<JsonNode> result = template.postForEntity(baseUrl+"2morse", translateRequest, JsonNode.class);
        Assertions.assertEquals(200, result.getStatusCodeValue());
        Assertions.assertEquals("\"-.-. .- .-. .- -.-. .- ... .-.-.-\"", result.getBody().get("text").toString() );
    }

    // Bit to Morse
    @Test
    void test_translate_bit_morse() {
        TranslateRequest translateRequest = new TranslateRequest();
        translateRequest.setText("000000001101101100111000001111110001111110011111100000001110111111110111011100000001100011111100000111111001111110000000110000110111111110111011100000011011100000000000");
        ResponseEntity<JsonNode> result = template.postForEntity(baseUrl+"bits2morse", translateRequest, JsonNode.class);
        Assertions.assertEquals(200, result.getStatusCodeValue());
        Assertions.assertEquals("\".... --- .-.. .- -- . .-.. ..\"", result.getBody().get("text").toString() );
    }

}